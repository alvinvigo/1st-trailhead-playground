public class ContactSearch {

    public static List<Contact> searchForContacts(String lastName, String postalCode) {
        // Create a list to store the results
        List<Contact> resultContacts = new List<Contact>();
        
        // Query for contacts with matching last name and mailing postal code
        resultContacts = [SELECT Id, Name FROM Contact WHERE LastName = :lastName AND MailingPostalCode = :postalCode];
        
        // Return the list of Contact records
        return resultContacts;
    }
}