public class AccountHandler {
    
    public static Account insertNewAccount(String accountName) {
        try {
            // Check if the incoming parameter is not empty
            if (String.isNotBlank(accountName)) {
                // Create a new Account instance
                Account newAccount = new Account(Name = accountName);
                
                // Insert the Account record
                insert newAccount;
                
                // If successful, return the inserted Account record
                return newAccount;
            }
        } catch (DmlException e) {
            // If a DML exception occurs, catch it and return null
            System.debug('An error occurred: ' + e.getMessage());
        }
        
        // If the parameter is empty or an exception occurred, return null
        return null;
    }
}