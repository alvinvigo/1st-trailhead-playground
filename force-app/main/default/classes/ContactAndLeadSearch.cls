public class ContactAndLeadSearch {

    public static List<List<sObject>> searchContactsAndLeads(String searchParam) {
        // Create a list to store the results
        List<List<sObject>> searchResults = new List<List<sObject>>();
        
        // Use SOSL to search for both contacts and leads based on the incoming parameter
        List<List<sObject>> searchList = [FIND :searchParam IN ALL FIELDS RETURNING Contact(Id, FirstName, LastName), Lead(Id, FirstName, LastName)];
        
        // Iterate through the search results to separate contacts and leads
        for (List<sObject> result : searchList) {
            List<sObject> contactResults = new List<sObject>();
            List<sObject> leadResults = new List<sObject>();
            
            for (sObject obj : result) {
                if (obj instanceof Contact) {
                    contactResults.add(obj);
                } else if (obj instanceof Lead) {
                    leadResults.add(obj);
                }
            }
            
            if (!contactResults.isEmpty()) {
                searchResults.add(contactResults);
            }
            
            if (!leadResults.isEmpty()) {
                searchResults.add(leadResults);
            }
        }
        
        // Return the list of Contact and Lead records
        return searchResults;
    }
}