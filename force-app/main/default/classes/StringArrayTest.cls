public class StringArrayTest {
    
    // Public static method to generate a list of formatted strings
    public static List<String> generateStringArray(Integer numberOfStrings) {
        List<String> result = new List<String>();
        
        // Loop to generate strings based on the input parameter
        for (Integer i = 0; i < numberOfStrings; i++) {
            // Format each string and add it to the list
            result.add('Test ' + i);
        }
        
        return result;
    }
}